# LineBotPhp

This is a simple LINE robot created with PHP.

```php
public function getChannelAccessToken(){
	//アクセストークン
	$channelAccessToken = "ACCESS TOKEN";
	return $channelAccessToken;
}
        
public function getChannelSecret(){
	//チャンネルシークレット
	$channelSecret = "CHANNEL SECRET";
	return $channelSecret;
}
```

# deploy

Build Web Server<br>
簡易的なウェブサーバーを作る

```bash
$ pwd
/Users/User_name/Path/LineBotPhp


$ php -S localhost:3333

```

expose a Local Server to Internet using ngrok<br>
ngrokを使用してローカルサーバーをインターネットに公開する

```bash
$ ls
LineBotPhp

$ ./ngrok http 3333

```

