<?php
require_once __DIR__ . '/lineBot.php';

// Linebotクラスをインスタンス化する
// Instantiate a Linebot class
$bot = new Linebot();

// ユーザーが入力されたテキストを返す関数を実行
// Execute a function that returns the text in which the user has entered
$text = $bot->getMessageText();

// ユーザーが入力されたテキストを引数に受け取ってreply関数を実行する
// The user receives the inputted text as retrieval and executes the reply function
$bot->reply($text);

